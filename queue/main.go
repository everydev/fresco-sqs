package main

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func handler(ctx context.Context, sqsEvent events.SQSEvent) error {
	for _, message := range sqsEvent.Records {
		fmt.Printf("The message %s for event source %s = %s \n", message.MessageId, message.EventSource, message.Body)

		c := Command{}
		err := json.Unmarshal([]byte(message.Body), &c)

		if err != nil {
			fmt.Println(err)
		}

		c.Run()
		c.RespondToSlack()

	}

	return nil
}

func main() {
	lambda.Start(handler)
}
